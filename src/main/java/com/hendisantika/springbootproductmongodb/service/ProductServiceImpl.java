package com.hendisantika.springbootproductmongodb.service;

import com.hendisantika.springbootproductmongodb.domain.Product;
import com.hendisantika.springbootproductmongodb.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-product-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-22
 * Time: 09:27
 */

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public Optional<Product> readById(String id) {
        return productRepository.findById(id);
    }

    @Override
    public List<Product> readAll() {
        return productRepository.findAll();
    }


    @Override
    public Product update(String id, Product product) {
        return null;
    }

    @Override
    public Product create(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void delete(String id) {
        productRepository.deleteById(id);

    }
}
