package com.hendisantika.springbootproductmongodb.repository;

import com.hendisantika.springbootproductmongodb.domain.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-product-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-22
 * Time: 09:06
 */
public interface ProductRepository extends MongoRepository<Product, String> {
}
