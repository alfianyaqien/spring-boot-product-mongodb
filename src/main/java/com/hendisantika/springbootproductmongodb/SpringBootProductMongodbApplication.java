package com.hendisantika.springbootproductmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootProductMongodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootProductMongodbApplication.class, args);
    }

}
