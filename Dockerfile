#FROM openjdk:21-slim
#FROM bellsoft/liberica-openjdk-debian:21
#FROM eclipse-temurin:21-jdk-alpine
FROM amazoncorretto:21-alpine-jdk
LABEL maintainer="hendisantika@yahoo.co.id"
WORKDIR /app
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=target/*.jar
#COPY ${JAR_FILE} product-1.0.0.jar
COPY target/*.jar /app/product-1.0.0.jar
ENTRYPOINT ["java","-jar","product-1.0.0.jar"]
